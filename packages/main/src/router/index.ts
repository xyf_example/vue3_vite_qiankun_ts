import { Router, createRouter, createWebHistory, createWebHashHistory } from "vue-router"
import microApps from "common/modules/microApps"
import Layout from "@/views/layout/Index.vue";

// 获取微服务路由
const microRoutes = <any>[]
microApps.forEach(micro => {
  microRoutes.push({
    path: `/${micro.name}/:morePath*`,
    component: Layout
  })
})

// console.log(microRoutes);
// 创建路由实例
export const router: Router = createRouter({
  history: createWebHashHistory(),
  routes: microRoutes.concat([
    {
      path: "/",
      component: Layout,
      redirect: "/micro_app1"
    }
  ]),
  scrollBehavior(to, from, savedPosition) {
    return new Promise(resolve => {
      if (savedPosition) {
        return savedPosition
      }
      if (from.meta.saveSrollTop) {
        const top: number = document.documentElement.scrollTop || document.body.scrollTop
        resolve({ left: 0, top })
      }
    })
  }
})

export default router
