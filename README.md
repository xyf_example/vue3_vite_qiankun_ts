# pnpm + vue3.0 + qiankun + Monorepo + typescript

## pnpm常用命令
### 1、`pnpm` 升级

```shell
pnpm add -g pnpm
```
### 2、配置仓库存储路径

```
pnpm config set store-dir /Users/xuyifei/Documents/pnpm-store
```

### 3、依赖安转和删除

```shell
pnpm add vue -S # 安装到全局
pnpm add axios --filter pkg1 # 安装到指定子应用

```


## vscode 扩展要求

> 安装这两个插件之后，也不需要原来的 *.vue 的类型声明了。

### 1、Volar 

提供了`vue`单文件组件和 typescript支持

### 2、vscode-typescript-vue-plugin 

用于支持在 `ts`中 导入 `.vue`文件

## 开发依赖安装

### 1、代码语法检查 

```shell
pnpm install eslint eslint-plugin-vue @typescript-eslint/eslint-plugin @typescript-eslint/parser eslint-define-config -D
```

- ESLint JavaScript 代码检查工具
- eslint-plugin-vue Vue 官方的 ESLint 插件，包含 vue/* 的规则集
- @typescript-eslint/eslint-plugin TS 检查的 ESLint 插件，包含 @typescript-eslint/* 的规则集
- @typescript-eslint/parser 帮助 ESLint 解析 TypeScript 语法

> 需要注意的 @typescript-eslint/parser 和 @typescript-eslint/eslint-plugin 版本号必需一致



添加 `.eslintrc.js`，`.eslintignore` 文件

`.eslintrc.js`
```js
// @ts-check

const { defineConfig } = require("eslint-define-config");

module.exports = defineConfig({
  root: true,
  env: {
    node: true,
    browser: true,
    es6: true
  },
  rules: {
    // rules...
  },
});

```
`.eslintignore`
```
node_modules
.DS_Store
dist
scripts
```

### 2、代码风格统一

```sh
pnpm install -D eslint-config-prettier eslint-plugin-prettier prettier -D
```

- eslint-config-prettier 关闭和 Prettier 冲突的 ESLint 规则
- eslint-plugin-prettier 把 Prettier 规则嵌入到 ESLint，下面代码的 prettier/prettier": "error"


修改 .eslintrc.js 配置

```
{
  "extends": [
    "some-other-config-you-use",
    "prettier"
  ],
  plugins: ["prettier"],
  rules: {
    "other-rules",
    "prettier/prettier": "error"
  }
}
```
### 3、Stylelint

```sh
pnpm install stylelint 
stylelint-config-recommended-vue stylelint-config-prettier stylelint-config-recess-order postcss-html stylelint-config-standard-scss -D
```

- Stylelint 强大、先进的 CSS 代码检查器，可以检查 CSS 代码中的错误和风格
- stylelint-config-recommended-vue Vue 的 Stylelint 的推荐规则集
- stylelint-config-prettier 关闭和 Prettier 冲突的规则
- stylelint-config-recess-order 对 CSS 属性进行排序
- postcss-html 解析 HTML 或者类 HTML 的 PostCSS 语法，比如 PHP、 VueSFC

新建 `.stylelintrc.json`

```json
{
  "extends": [
    "stylelint-config-standard-scss",
    "stylelint-config-recommended-vue/scss",
    "stylelint-config-prettier",
    "stylelint-config-recess-order"
  ]
}
```

## 上产依赖安装

```sh
pnpm install @ant-design/icons-vue ant-design-vue @vueuse/core pinia vue-router vue
```
 ## monorepo

 ### 1、配置文件添加

 新建 `pnpm-workspace.yaml`

```yaml
prefer-workspace-packages: true
packages:
  - 'packages/**'
```
这样 `packages` 里就是单独的 仓库，而各种配置和依赖可以共用全局的


## qiankun

### 1、 为了让共享配置层级清晰，qiankun 主应用（基座）和子应用都放到 packages 中

```sh
pnpm install qiankun  
```

### 2、nginx 配置

```conf
server {
    listen       10003;   # 主应用监听的端口号
    server_name  localhost;    # 配置的域名，目前是本地测试，所以直接使用 localhost
    
    location / {
      alias   /xxx/dist/main/;  # 网站根目录，这里选用主应用构建后的文件目录
      index  index.html index.htm;   # 默认首页文件
      try_files $uri $uri/ /index.html;
      expires -1;                          # 首页一般没有强制缓存
    }
  }

  server {
    listen       10001;   # 配置监听的端口
    server_name  localhost;    # 配置的域名，目前是本地测试，所以直接使用 localhost
    
    # 配置跨域访问，此处是通配符，严格生产环境的话可以指定为主应用 192.168.2.192:8889
    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
    add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';

    location / {
      alias   /xxx/dist/app1/;  # 网站根目录，这里选用主应用构建后的文件目录
      index  index.html index.htm;   # 默认首页文件
      try_files $uri $uri/ /index.html;
      expires -1;                          # 首页一般没有强制缓存
    }
  }

    server {
    listen       10002;   # 配置监听的端口
    server_name  localhost;    # 配置的域名，目前是本地测试，所以直接使用 localhost
    
    # 配置跨域访问，此处是通配符，严格生产环境的话可以指定为主应用 192.168.2.192:8889
    add_header Access-Control-Allow-Origin *;
    add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
    add_header Access-Control-Allow-Headers 'DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization';

    location / {
      alias   /xxx/dist/app2/;  # 网站根目录，这里选用主应用构建后的文件目录
      index  index.html index.htm;   # 默认首页文件
      try_files $uri $uri/ /index.html;
      expires -1;                          # 首页一般没有强制缓存
    }
  }
```

### 3、页面打包加载不出来
修改`vite.config.js`中的`base`字段
```js
base: mode =='local'?'/':'http://localhost:10002/',
```

## 整合配置

### 1、直接安装所有依赖
```sh
pnpm ins
```

### 2、直接运行所有项目
```sh
pnpm -r --filter micro_* --parallel run dev
```

注意 `micro_*` 这个是对应每个项目的 package.json 中的 `name`属性，最好有一个统一的标志，方便到时候进行筛选

## 常见问题

#### 1、 "在配置文件“/Users/xuyifei/Documents/study/pnpm_ts_qiankun/tsconfig.json”中找不到任何输入。指定的 \"include\" 路径为“[\"src/**/*.ts\",\"src/**/*.tsx\",\"src/**/*.vue\",\"types/*.d.ts\",\"mock/*.ts\",\"packages/ng-df/vite.config.ts\",\"auto-imports.d.ts\",\"components.d.ts\",\"packages/**/*.vue\",\"packages/**/*.ts\",\"common/**/*.ts\",\"common/**/*.tsx\",\"common/**/*.vue\"]”，\"exclude\" 路径为“[\"dist\",\"**/*.js\"]”。",

添加 `auto-imports.d.ts` 文件









