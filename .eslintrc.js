// @ts-check

const { defineConfig } = require("eslint-define-config");

module.exports = defineConfig({
  root: true,
  env: {
    node: true,
    browser: true,
    es6: true,
  },
  extends: ["some-other-config-you-use", "prettier"],
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error",
  },
});


