type microApp = {
  name: string;
  entry: string;
  container: "#view-main";
  activeRule: string;
  port: number | string;
};



const apps: microApp[] = [
  {
    name: "micro_app1",
    entry: "//localhost:10001",
    container: "#view-main",
    activeRule: "/#/micro_app1",
    port: "10001",
  },
  {
    name: "micro_app2",
    entry: "//localhost:10002",
    container: "#view-main",
    activeRule: "/#/micro_app2",
    port: "10002",
  },
];
export default apps;
